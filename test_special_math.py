import pytest
from app.helpers import calc_special_math

def test_special_math_response():
    assert calc_special_math(7) == 79
    assert calc_special_math(17) == 10926
