from .helpers import calc_special_math
from app import app
from flask import request, url_for


@app.route('/')
@app.route('/index')
@app.route('/special_math/')
def index():
    return  f'access special_math endpoint with /special_math/&lt;number&gt; <br /><br />e.g.: {request.base_url[:-1]}{url_for("special_math", num=7)}'


@app.route('/special_math/<int:num>')
def special_math(num):
    return str(calc_special_math(num))
