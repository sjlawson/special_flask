

def calc_special_math(num=0):
    return num if num == 0 or num == 1 else num + calc_special_math(num-1) + calc_special_math(num-2)
