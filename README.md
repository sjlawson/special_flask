# special_flask

Sample project for interviews demonstrating a little basic python usage for an API

To run app from a docker container:

1. `docker build -t special_math https://gitlab.com/sjlawson/special_flask.git#main`
2. `docker run -p 5000:5000 special_math`
3. Watch for the URL in the run dialogue and run the app in your browser!
