FROM python:3.9-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get update
RUN apt-get install -y git
RUN git clone https://gitlab.com/sjlawson/special_flask.git
RUN pip install --upgrade pip
WORKDIR /special_flask

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["flask", "run", "-h", "0.0.0.0"]